# docker-reverse-proxy
nginx-proxyを使用したリバースプロキシ

## Description
[jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy/)を使用したdocker-compose。  
リバースプロキシ経由でDockerコンテナへのホスト名でのアクセスを可能にする。

## Usage
##### 1. 共通ネットワークを作成する

```sh
docker network create --driver bridge common_link
```

##### 2. バーチャルホストを設定する

```yml
version: '2'
services:
  xxxx:
    image: xxxx
    container_name: xxxx
    environment:
      - VIRTUAL_HOST=xxxx.example.test
      - VIRTUAL_PORT=8080
    ports:
      - "8080"
networks:
  default:
    external:
      name: common_link
```

##### 3. hostsファイルに登録する  


```
# DockerホストのIPアドレス：192.168.56.130
192.168.56.130 xxxx.example.test
```

## 参考
- [nginxを使ったリバースプロキシ on Docker](https://qiita.com/ka2asd/items/372d30be64c57a8a81b1)
- [リバースプロキシを使ってDockerコンテナへのアクセスをポート番号でなくホスト名で分ける](https://qiita.com/osuo/items/6799cefb0aeb25da95ae)
- [vagrant + docker-compose + リバースプロキシでマルチな環境開発構築](http://walking-succession-falls.com/vagrant-docker-compose-%E3%83%AA%E3%83%90%E3%83%BC%E3%82%B9%E3%83%97%E3%83%AD%E3%82%AD%E3%82%B7%E3%81%A7%E3%83%9E%E3%83%AB%E3%83%81%E3%81%AA%E7%92%B0%E5%A2%83%E9%96%8B%E7%99%BA%E6%A7%8B%E7%AF%89/)
